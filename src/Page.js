import React from "react";
import Header from "./components/header/Header";
import Main from "./components/main/main";
import Footer from "./components/footer/Footer";

function Page() {
  return (
    <>
      <Header title="Header title" />
      <Main />
      <Footer title="Footer title" />
    </>
  );
}

export default Page;
