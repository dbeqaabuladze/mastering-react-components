import React from "react";
import { Photo } from "../../data";
import "./stylephotos.css";

function Photos({ photos }) {
  return (
    <div>
      <h2>Photos</h2>
      <ul>
        {Photo.map((photos) => (
          <li key={photos.id}>
            <img src={photos.url} alt={photos.title} />
            <p>{photos.title}</p>
          </li>
        ))}
      </ul>
    </div>
  );
}
export default Photos;
