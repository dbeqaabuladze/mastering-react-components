import React from "react";
import Photos from "../photos/Photos";
import "./main.css";

function Main() {
  return (
    <div className="mainContent">
      <Photos />
    </div>
  );
}

export default Main;
