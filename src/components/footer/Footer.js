import React from "react";
import "./footer.css";
function Footer({ title = "footer title" }) {
  return <footer className="footer">{title}</footer>;
}
export default Footer;
