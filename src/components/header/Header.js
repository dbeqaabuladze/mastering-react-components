import PropTypes from "prop-types";
import "./header.css";

function Header({ title }) {
  return <header className="header">{title}</header>;
}

Header.propTypes = {
  title: PropTypes.string.isRequired,
};

export default Header;
